const { CookieShield, PolicyShield } = require('@forgerock/openam-agent');
const agent = require('./openam-agent');
const agentConfig = require('../config/agent-config');

const cookieShield = new CookieShield({
    getProfiles: true,
    noRedirect: true
});
const cookieMiddleware = agent.shield(cookieShield);

const cookieShieldPass = new CookieShield({
    passThrough:true,
    getProfiles: true
});
const cookieMiddlewarePass = agent.shield(cookieShieldPass);


policyShield = new PolicyShield(agentConfig.appName);
const policyMiddleware = agent.shield(PolicyShield);


module.exports = { cookieMiddlewarePass, cookieMiddleware, policyMiddleware };