const openAM = require('@forgerock/openam-agent')
const openAMConfig = require('../config/agent-config');

module.exports = new openAM.OpenAMClient(openAMConfig.serverUrl);