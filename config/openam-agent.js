const ejs = require('ejs');
const { PolicyAgent } = require('@forgerock/openam-agent');
const agentConfig = require('./agent-config.js');
const path = require('path');
const fs = require('fs');

const policyAgent = new PolicyAgent(
    {
        ...agentConfig,
        errorPage: function (status, message, details) {
            console.log(status.status, status.message, status.details);
            return ejs.render(fs.readFileSync(path.join(__dirname, '../views/login.ejs'), { encoding: 'utf-8' }), { reason: 401 });
        }
    });

module.exports = policyAgent;