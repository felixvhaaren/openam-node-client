const express = require('express');
const router = express.Router();
const openAMClient = require('../config/openam-client');
const { cookieMiddleware, cookieMiddlewarePass } = require('../config/openam-policies');
const agentConfig = require('../config/agent-config');
const PostModel = require('../models/post.model');


/**
 * Render Home
 */
router.get('/',cookieMiddlewarePass, function (req, res) {
  console.log(req.session);
  res.render('index', { template: "partials/home",username: req.session.data.uid});
});

/**
 * Render Login
 */
router.get('/login', function (req, res) {
  res.render('login', { reason: 200 });
});

/**
 * Redirect to Open AM Login
 */
router.get('/loginAM', function (req, res) {
  res.send({ url: openAMClient.getLoginUrl('http://www.example.com:8000/blog') });
});

/**
 * Render Blog - Protected
 */
router.get('/blog', cookieMiddleware, async function (req, res, next) {
  console.log(req.session);
  const userProfile = await openAMClient.getProfile(req.session.data.uid, agentConfig.realm, req.session.key, 'iPlanetDirectoryPro');
  PostModel.find({}, function (err, posts) {
    if (err) next(err);
    res.render('index', { template: "pages/blog", username: userProfile.cn, posts: posts });
  });
});

module.exports = router;
