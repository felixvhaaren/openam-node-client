const express = require('express');
const router = express.Router();
const PostModel = require('../models/post.model');
const { cookieMiddleware, policyMiddleware } = require('../config/openam-policies');
const openAMClient = require('../config/openam-client');


/**
 * POST - Login
 */
router.post('/login', function (req, res) {
    console.log(req.body);
    if (req.body.username === "" || req.body.password === "") {
        res.status(401);
    } else {
        openAMClient.authenticate(req.body.username, req.body.password).then(data => {
            res.cookie('iPlanetDirectoryPro', data.tokenId, { domain: '.example.com' });
            res.status(200).send({ redirect: "/blog" });
        }).catch(err => {
            res.status(err.response.status).send();
        });
    }
});


/**
 *  Open AM Middleware - Cookie Shield - everything protected from here
 */
router.use(cookieMiddleware);

/**
 * POST - add Post
 */
router.post('/post', function (req, res, next) {
    console.log(req.body, req.session);
    var Post = new PostModel({
        username: req.session.data.uid,
        content: req.body.content
    });

    Post.save(function (err) {
        if (err) throw err;
        res.status(200).send({ message: "OK" });
    });
});

/**
 * PUT - edit Post
 */

router.put('/post', async function (req, res) {
    console.log(req.body);
    var Post = new PostModel();

    PostModel.findOneAndUpdate(req.body.post_id, {
        username: req.session.data.uid,
        content: req.body.content
    }, { new: true }, function (err, model) {
        console.log(err, model);
        if (err) throw err;
        res.status(200).send({ message: "OK" });
    });
});

/**
 * DELETE - delete POST
 */
router.delete('/post', async function (req, res) {
    console.log("HERE");
    PostModel.deleteOne({ _id: req.body.post_id }, function (err) {
        if (err) throw err;
        res.status(200).send({ message: "OK" });
    });
});

module.exports = router;