jQuery(document).ready(() => {
    /**
     * Send Login to Server
     */
    jQuery('#loginForm').submit(function (e) {
        e.preventDefault();
        jQuery.ajax({
            method: "POST",
            url: "/api/login",
            data: {
                username: $('#username').val(),
                password: $('#password').val(),
            },
            dataType: "json",
            success: data => {
                console.log(data);
                if (data.redirect) {
                    window.location.href = data.redirect;
                }
            },
            error: (jqXHR, textStatus, errorThrown) => {
                $("#loginButton").text("Wrong password");
                $("#loginButton").addClass("btn-gradient-danger").removeClass("btn-gradient-primary");
                $("#password").val('');
                setTimeout(() => {
                    $("#loginButton").text("Login");
                    $("#loginButton").removeClass("btn-gradient-danger").addClass("btn-gradient-primary");
                }, 2500);
            }
        });
    });

    /**
     * Request for OpenAM Login
     */
    jQuery('#loginWithOpenAM').click(function (e) {
        jQuery.ajax({
            method: "GET",
            url: "/loginAM",
            dataType: "json",
            success: data => {
                console.log(data);
                if (data.url) {
                    window.location.href = data.url;
                }
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.error(jqXHR, textStatus, errorThrown)
            }
        });
    });

    /**
     * Open Modal to add a post
     */
    jQuery('#addPost').click(function (e) {
        $('#postModal #postContent').attr('placeholder', 'Your message here...');
        $('#postIdModal').text('');
        $('#postModal').modal("show");
    });

    /**
     * Open Modal to edit a post
     */
    jQuery('.profile-feed-item').on('click', '#editPost', function (e) {
        console.log("HERER");
        const postId = $(this).data('postid');
        $('#postModal #username').val($(this).parent().parent().find('h6').text())
        $('#postModal #postContent').val($(this).parent().parent().find('#postContent').text());
        console.log(postId);
        $('#postIdModal').text(postId);
        $('#postModal').modal("show");
    });

    /**
     * On Click Cancel
     */
    jQuery('#cancelModal, .close').click(function (e) {
        $('#postModal').modal("hide");
    });

    /**
     * On Add/Edit Post
     */
    jQuery('#submitPost').click(function (e) {
        if (!$('#postIdModal').text()) {
            jQuery.ajax({
                method: "POST",
                url: "/api/post",
                dataType: "json",
                data: {
                    content: $.trim($("#postModal #postContent").val())
                },
                success: () => {
                    window.location.reload();
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.error(jqXHR, textStatus, errorThrown)
                }
            });
        } else {
            jQuery.ajax({
                method: "PUT",
                url: "/api/post",
                dataType: "json",
                data: {
                    post_id: jQuery('#postIdModal').text(),
                    content: $.trim($("#postModal #postContent").val())
                },
                success: () => {
                    window.location.reload();
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.error(jqXHR, textStatus, errorThrown)
                }
            });
        }
    });

    /**
     * Delete Post
     */
    jQuery('.profile-feed-item').on('click', '#deletePost', function (e) {
        console.log("HERE");
        jQuery.ajax({
            method: "DELETE",
            url: "/api/post/",
            dataType: "json",
            data: {
                post_id: $(this).data('postid')
            },
            success: () => {
                window.location.reload();
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.error(jqXHR, textStatus, errorThrown)
            }
        });
    });
});