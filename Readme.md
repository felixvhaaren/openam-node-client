# Open AM Node.js Client

This is a OpenAM-Node-Client. It connects to http://www.example.com:8000 where an OpenAM instance is setup.

## Install

```
npm install
```

## Usage
```
npm start
```

## Contributing

PRs accepted.

## License

MIT © Felix v. Haaren & Sascha Blaum